# Installation
> `npm install --save @types/unorm`

# Summary
This package contains type definitions for unorm (https://github.com/walling/unorm).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/unorm.

### Additional Details
 * Last updated: Fri, 15 May 2020 13:12:50 GMT
 * Dependencies: none
 * Global values: `unorm`

# Credits
These definitions were written by [Christopher Brown](https://github.com/chbrown).
